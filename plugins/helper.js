const jwt = require('jsonwebtoken')

module.exports = {

    _error: (ex, swal = false) => {

        let error = '';

        if (ex.response) {

            if (ex.response.data) {

                if (ex.response.data.data) {

                    if (ex.response.data.data.message) {

                        error = ex.response.data.data.message;

                    } else {

                        error = ex.response.data.data;
                    }

                } else {

                    error = ex.response.data.message;
                }
            }

        } else {

            error = ex.message;
        }

        return swal
            ? { type: 'error', title: 'Oops...', text: error }
            : error;
    },

    _dashboard_url: (dashboard_id = 0) => {

        const METABASE_SITE_URL = "https://metabase-reporting.herokuapp.com";
        const METABASE_SECRET_KEY = "2eb96bee291774785693831f774fd9e60bf8c0412edf5daea14917f681b6d73c";

        var payload = {

            resource: { dashboard: dashboard_id },
            params: {},
            exp: Math.round(Date.now() / 1000) + (1 * 60 * 60)
        };

        var token = jwt.sign(payload, METABASE_SECRET_KEY);

        return `${METABASE_SITE_URL}/embed/dashboard/${token}#bordered=true&titled=false`;

    },

    _cloudinary_settings: {

        cloudName: 'ebukailoh',
        uploadPreset: 'damier_spaces',
        multiple: false,
        maxFiles: 1,
        cropping: false,
        clientAllowedFormats: ['png', 'jpeg', 'jpg']
    },

    _website_url: 'https://damier.herokuapp.com'
    //_website_url: 'http://localhost:8000'

}