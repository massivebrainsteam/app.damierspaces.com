let new_space = {

    category_id : '',
    location_id: '',
    package_id: '',
    name: '',
    capacity: 1,
    photo_url: '',
    amenities: [],
    photos: [],
    rules: '',
    usage: '',
    start_at: '8:00 AM',
    end_at: '5:00 PM'

}

export const state = () => ({

    new_space : new_space
    
});

export const mutations = {

    updateNewSpace (state, payload){

        if(!payload)
            return state.new_space = new_space;

        state.new_space = {...state.new_space, ...payload};
    }
}