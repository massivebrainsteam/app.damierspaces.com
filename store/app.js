let app = {

    user : {},
    active: 'dashboard',
    title: 'Dashboard',
    description: 'Welcome!',
    button: null,
    button_url: null,
    button_icon: 'plus',
    sidebar: true

}

export const state = () => ({

    app : app
    
});

export const mutations = {

    updateApp (state, payload){

        if(!payload)
            return state.app = app;

        state.app = {...state.app, ...payload };
    }
}