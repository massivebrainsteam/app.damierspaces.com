let new_host = {

    name : '',
    description: '',
    email: '',
    phone: '',
    address: '',
    contact_name: '',
    contact_email: '',
    contact_phone: ''

}

export const state = () => ({

    new_host : new_host
    
});

export const mutations = {

    updateNewHost (state, payload){

        if(!payload)
            return state.new_host = new_host;

        state.new_host = {...state.new_host, ...payload};
    }
}