
export default {

  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: 'Damier Spaces',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'description', content: 'Damier Spaces - Enjoy frictionless daily access to the best workspaces near you!' },
      { name: 'author', content: 'Olaiya Segun' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/img/brand/favicon.png' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/fontawesome5/css/all.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/flatpickr.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/style.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/swiper/swiper.min.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/damier.css' },
      { rel: 'stylesheet', type: 'text/css', href: '/css/tooltip.css' },
      { rel: 'stylesheet', type: 'text/css', href: 'https://fonts.googleapis.com/css?family=Poppins&display=swap' }
    ],
    script: [
      { src: '/js/damier.core.js' },
      { src: '/swiper/swiper.min.js' },
      { src: '/js/custom.js' },
    ],
    bodyAttrs: {

      class: 'application application-offset ready'
    }

  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },
  /*
  ** Global CSS
  */
  css: [
  ],
  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/components',
    '~/plugins/vue-sweetalert',
    '~/plugins/vue-tooltip',
    '~/plugins/vue-lightbox',
    '~/plugins/helper',
  ],
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
  ],
  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    '@nuxtjs/auth',
    '@nuxtjs/proxy'
  ],
  /*
  ** Axios module configuration
  ** See https://axios.nuxtjs.org/options
  */
  axios: {

    //baseURL: '/api'
    baseURL: 'https://damier.herokuapp.com/api'

  },

  proxy: {

    '/api': 'http://localhost:8000'
  },

  auth: {

    strategies: {

      local: {

        endpoints: {

          login: { url: '/auth/login', method: 'post', propertyName: 'data.api_token' },
          user: { url: '/auth/profile', method: 'get', propertyName: 'data' },
          logout: false
        }
      }
    },

    redirect: {

      login: '/',
      logout: '/',
      home: '/'
    }
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
    }
  },

  router: {

    middleware: ['auth']
  }
}
